import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCircle } from '@fortawesome/free-solid-svg-icons'
import { faArrowCircleUp } from '@fortawesome/free-solid-svg-icons'
import { faArrowCircleDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


import  envWidget   from "./components/envWidget";


library.add(faCircle);
library.add(faArrowCircleUp);
library.add(faArrowCircleDown);


Vue.use(Buefy);
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('env-widget', envWidget)



new Vue({
  el: '#app',
  render: h => h(App),
});